Example [Zola](https://www.getzola.org/) website using [Gitlab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

Learn more about GitLab Pages at the [official documentation](https://docs.gitlab.com/ee/user/project/pages/).

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
    ```shell
    git clone --recurse-submodules <clone url>
    ```
2. [Install](https://www.getzola.org/documentation/getting-started/installation/) Zola.
3. Preview your project: #TODO
    ```shell
    zola serve
    ```
4. Add content: https://www.getzola.org/documentation/content/overview/
5. Optional. Generate the website:
    ```shell
    zola build
    ```
## Use a custom theme

Zola supports a variety of themes.

Visit <https://www.getzola.org/themes/> and pick the theme you want to use. In the
Pages example, we use <https://www.getzola.org/themes/hyde/>.

### Use a custom theme using a git submodule

The example [`.gitlab-ci.yml`](.gitlab-ci.yml) uses a git submodule to import the theme.

To use your own theme:

1. Navigate to the `themes/` diretory: `cd themes/`
2. Add the theme you want as a submodule:
    ```shell
    git submodule add https://github.com/getzola/hyde.git
    ```
3. Edit `config.toml` and add the theme:

```plaintext
theme = "hyde"
```

## GitLab User or Group Pages

To use this project as your user/group website, you will need to perform
some additional steps:

1. Rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings > General (Advanced)**.
1. Change the `base_url` setting in your `config.toml`, from `"https://pages.gitlab.io/zola"` to `base_url = "https://namespace.gitlab.io"`.
Proceed equally if you are using a custom domain: `base_url = "https://example.com"`.

Read more about [GitLab Pages for projects and user/group websites](https://docs.gitlab.com/ce/user/project/pages/getting_started_part_one.html).

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.
