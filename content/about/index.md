+++
title = "About"
description = "This is the about section"
date = "2023-05-01"
+++

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Zola](https://www.getzola.org/), and can be built in under 1 minute.
Literally. It uses the [`hyde` theme](https://github.com/getzola/hyde)
which supports content on your front page.
Edit `/content/about/index.md` to change what appears here. Delete `/content/about/index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/zola) to get started.
